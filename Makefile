# Build a Hello World C program

CC     = gcc
CFLAGS = -g -Wall

TARGET = main 

all: $(TARGET)

main: main.c
	$(CC) $(CFLAGS) -o $(TARGET) main.c

clean:
	rm $(TARGET)

